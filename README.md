# LINMOBapps

## LINux on MOBile Apps

### History
This project was served at [https://linmobapps.frama.io/](https://linmobapps.frama.io/) and has been forked from the great [https://mglapps.frama.io/](https://mglapps.frama.io) list always with the goal of creating something static site generator based with the name "LinuxPhoneApps.org". On March 28th, 2022, after more than one year of interim LINMOBapps existence, [LinuxPhoneApps.org](https://linuxphoneapps.org) was finally launched. 

### Present
Files:
* [index.html](index.html): Main page hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/), redirects to LinuxPhoneApps.org now. If you really miss the old page, e.g. for its better search functionality, fear not, it still lives on [LinuxPhoneApps.org](https://linuxphoneapps.org/lists/apps-classic/)! 
* [complete.csv](complete.csv): Complete list of apps, to maintain remerge-ability to MGLapps, no longer maintained,
* __[apps.csv](apps.csv)__: Main app list (subset of complete.csv), to be edited directly. _Add your apps here ([instructions](https://linuxphoneapps.org/docs/contributing/edit-csv-in-libreoffice/))!_ 
* __[apps-to-be-added.md](apps-to-be-added.md)__: Apps waiting to be added.
* [games.html](games.html): Page to display the Game List hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/games.html)
* __[games.csv](games.csv)__: Main games list (subset of complete.csv), to be edited directly.
* [archive.html](archive.html): Page to display the Archive List hosted at [https://linmobapps.frama.io/](https://linmobapps.frama.io/archive.html)
* [archive.csv](archive.csv): Retired apps (subset of complete.csv).
* [other games.csv](other apps.csv): Further games which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the main list

Just like its origin [MGLApps](https://mglapps.frama.io), LINux on MOBile Apps is licensed under CC BY-SA 4.0 International: [https://creativecommons.org/licenses/by-sa/4.0/](https://creativecommons.org/licenses/by-sa/4.0/) . For more licensing information, see [LICENSE](LICENSE)

### My app is missing!
* If you have an app that might work on LinuxPhones, but don't have a device, [please head over here](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/12)!
* If an app works for you and it's not listed yet, [head over there](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/13)! If you don't want to sign up for Framagit (which is weird, because [FramaSoft](https://framasoft.org/en/) is really cool), you can also just [submit your app by email](https://linuxphoneapps.org/docs/contributing/submit-app-by-email/) and hope some contributor gets to it quickly :)

### Contributing
If you want to help, check [apps-to-be-added.md](apps-to-be-added.md) for a list of known apps that might be work adding or just check the [open Issues](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues) for improvements to existing listings and feel free to open some new ones! 



### Past: Removed content / experimental packaging scripts

For a while this repo contained a few PKGBUILD scripts (for building packages on Arch or Manjaro) and an APKBUILD (for postmarketOS) scripts.
These have since been moved to their own repositories:

* [APKBUILDs](https://framagit.org/linmobapps/apkbuilds)
* [PKGBUILDs](https://framagit.org/linmobapps/pkgbuilds)
* [Tweaks](https://github.com/1peter10/linuxphone-tweaks)
